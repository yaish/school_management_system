# Installation

```sh
- git clone https://gitlab.com/yaish/school_management_system.git
- create virtualenv
- pip install -r requirements
- bower install
```
# Create postgres database
```
- this is my default db settings,  you can edit with your own credentials

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'sms'
            'USER': 'galyaish',
            'PASSWORD': '123456',
            'HOST': 'localhost',
            'PORT': '',
        }
    }

- create psql db createdb <db_name>
- python manage.py migrate 
```

# Run

```sh
- python manage.py runserver
- python manage.py generate_data (custom management command to generate models and relations)
```


# API Reference

```sh
- Django Rest Framework 127.0.0.1:8000/api/v1/sms-app/
- Application website  127.0.0.1:8000/api/v1/sms-app/view
```

