# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Avg


class School(models.Model):
    LEVELS = (
        (1, 'EASY'),
        (2, 'MEDIUM'),
        (3, 'HARD'),
    )

    name = models.CharField(default='', max_length=256)
    school_level = models.IntegerField(choices=LEVELS, default=1)

    def get_average(self, ):
        try:
            return sum(
                [class_obj.get_average() for class_obj in self.school_classes.all()]) / self.school_classes.count()
        except ZeroDivisionError:
            return 0

    def __str__(self):
        return 'School: {0}, Level: {1}'.format(self.name, unicode(self.school_level))


class Class(models.Model):
    name = models.CharField(default='', max_length=256)
    school = models.ForeignKey(School, blank=True, null=True, related_name='school_classes')

    def get_average(self, ):
        try:
            return sum(student.get_average() for student in self.class_students.all()) / self.class_students.count()
        except ZeroDivisionError:
            return 0

    def __str__(self):
        return 'Class: {0}'.format(self.name)


class BaseUser(models.Model):
    user = models.OneToOneField(User, blank=True, null=True)
    birth_date = models.DateField(null=True, blank=True)
    telephone = models.CharField(default='', max_length=256)
    cellphone = models.CharField(default='', max_length=256)
    address = models.CharField(default='', max_length=256)
    gender = models.CharField(max_length=1, choices=(('M', 'Male'), ('F', 'Female')), default='M')

    def __str__(self):
        return self.user.first_name + ' ' + self.user.last_name


class StudentUser(BaseUser):
    activities = models.ManyToManyField('Activity', null=True, blank=True, through='ActivityData')
    current_class = models.ForeignKey(Class, null=True, blank=True, related_name='class_students')

    def get_average(self, ):
        avg = self.student_activity_data.aggregate(Avg('score')).values()[0]
        if avg is not None:
            return avg
        return 0

    def get_user_tag(self):
        return 'S'

    def __str__(self):
        return self.user.first_name + ' ' + self.user.last_name

    class Meta:
        verbose_name = 'StudentUser'


class TeacherUser(BaseUser):
    teaching_classes = models.ManyToManyField(Class, null=True, blank=True, related_name='class_teachers')
    is_manager = models.BooleanField(default=False)

    def __str__(self):
        return self.user.first_name + ' ' + self.user.last_name

    def get_user_tag(self):
        return 'T'

    class Meta:
        verbose_name = 'TeacherUser'


class ManagerUser(TeacherUser):
    school = models.ForeignKey(School, blank=True, null=True)

    def __str__(self):
        return self.user.first_name + ' ' + self.user.last_name

    def get_user_tag(self):
        return 'M'

    def save(self, *args, **kwargs):
        self.is_manager = True
        super(ManagerUser, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'ManagerUser'


class Activity(models.Model):
    ACTIVITY_TYPES = (
        ('Sport', 'Sport'),
        ('Bible', 'Bible'),
        ('Dance', 'Dance'),
        ('Math', 'Math'),
        ('Literature', 'Literature'),
        ('Geography', 'Geography'),
    )

    activity_type = models.CharField(max_length=256, choices=ACTIVITY_TYPES, default='Sport')
    description = models.TextField(default='', max_length=256)

    def __str__(self):
        return self.activity_type


class ActivityData(models.Model):
    student = models.ForeignKey('StudentUser', blank=True, null=True, related_name='student_activity_data')
    activity = models.ForeignKey(Activity, blank=True, null=True, related_name='activity_data')
    date = models.DateTimeField(null=True, blank=True)
    score = models.IntegerField(default=0)

    def __str__(self):
        return 'activity: {0}, date: {1}, score: {2}'.format(self.activity, self.date.strftime('%d/%m/%Y'), self.score)

    def save(self, *args, **kwargs):
        if self.score == 80 and self.student is not None and self.student.current_class is not None and self.student.current_class.school is not None:
            self.score = 80 + (self.student.current_class.school.school_level - 1) * 5
        super(ActivityData, self).save(*args, **kwargs)

