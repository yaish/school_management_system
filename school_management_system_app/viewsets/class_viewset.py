from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from school_management_system_app.models import Class
from school_management_system_app.serializers.class_serializer import ClassSerializer


class ClassViewSet(NestedViewSetMixin, ModelViewSet):
    queryset = Class.objects.all()
    model = Class
    serializer_class = ClassSerializer
