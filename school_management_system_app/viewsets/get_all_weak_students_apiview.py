from django.db.models import Avg
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from school_management_system_app.models import StudentUser, ActivityData
from school_management_system_app.serializers.student_user_serializer import StudentUserSerializer


class WeakStudentsApiView(APIView):
    def get(self, request):
        weak_students = StudentUser.objects.filter(id__in=map(lambda x: x['student_id'], filter(lambda x: x['average'] <= 75,
                                                                                ActivityData.objects.values(
                                                                                    'student_id').annotate(
                                                                                    average=Avg('score')))))
        serializer = StudentUserSerializer(weak_students.all(), many=True)
        try:
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        except:
            return Response(data=[], status=status.HTTP_500_INTERNAL_SERVER_ERROR)


