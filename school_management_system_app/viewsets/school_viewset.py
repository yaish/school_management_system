from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from school_management_system_app.models import School
from school_management_system_app.serializers.school_serializer import SchoolSerializer


class SchoolViewSet(NestedViewSetMixin, ModelViewSet):
    queryset = School.objects.all()
    model = School
    serializer_class = SchoolSerializer
