from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from school_management_system_app.models import Activity
from school_management_system_app.serializers.activity_serializer import ActivitySerializer


class ActivityViewSet(NestedViewSetMixin, ModelViewSet):
    queryset = Activity.objects.all()
    model = Activity
    serializer_class = ActivitySerializer
