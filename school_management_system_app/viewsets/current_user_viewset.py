from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from school_management_system_app.models import StudentUser, ManagerUser, TeacherUser
from school_management_system_app.serializers.manager_user_serializer import ManagerUserSeralizer
from school_management_system_app.serializers.student_user_serializer import StudentUserSerializer
from school_management_system_app.serializers.teacher_user_serializer import TeacherUserSeralizer

class CurrentUserViewSet(APIView):
    def get(self, request, format=None):


        current_user = request.user
        manager_user = ManagerUser.objects.filter(user=current_user, is_manager=True)
        student_user = StudentUser.objects.filter(user=current_user)
        teacher_user = TeacherUser.objects.filter(user=current_user, is_manager=False)
        if manager_user:
            serializer = ManagerUserSeralizer(manager_user[0])
        elif teacher_user:
            serializer = TeacherUserSeralizer(teacher_user[0])
        elif student_user:
            serializer = StudentUserSerializer(student_user[0])
        else:
            return Response(data=[], status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

