from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from school_management_system_app.models import StudentUser
from school_management_system_app.serializers.student_user_serializer import StudentUserSerializer


class StudentUserViewSet(NestedViewSetMixin, ModelViewSet):
    queryset = StudentUser.objects.all()
    model = StudentUser
    serializer_class = StudentUserSerializer
