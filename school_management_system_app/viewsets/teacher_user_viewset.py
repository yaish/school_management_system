from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from school_management_system_app.models import TeacherUser
from school_management_system_app.serializers.teacher_user_serializer import TeacherUserSeralizer


class TeacherUserViewSet(NestedViewSetMixin, ModelViewSet):
    queryset = TeacherUser.objects.filter(is_manager=False)
    model = TeacherUser
    serializer_class = TeacherUserSeralizer
