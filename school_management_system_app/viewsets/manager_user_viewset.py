from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from school_management_system_app.models import ManagerUser
from school_management_system_app.serializers.manager_user_serializer import ManagerUserSeralizer


class ManagerUserViewSet(NestedViewSetMixin, ModelViewSet):
    queryset = ManagerUser.objects.filter(is_manager=True)
    model = ManagerUser
    serializer_class = ManagerUserSeralizer
