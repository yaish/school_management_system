from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from school_management_system_app.models import ActivityData
from school_management_system_app.serializers.activity_data_serializer import ActivityDataSerizlier


class ActivityDataViewSet(NestedViewSetMixin, ModelViewSet):
    queryset = ActivityData.objects.all()
    model = ActivityData
    serializer_class = ActivityDataSerizlier
