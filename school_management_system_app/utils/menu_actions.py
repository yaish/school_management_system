from __future__ import division
import os, sys
from school_management_system_app.utils.model_creator import ModelCreator
from school_management_system_app.models import Class, StudentUser, TeacherUser, ManagerUser, School, Activity, ActivityData
from django.contrib.auth import get_user_model
User = get_user_model()
import datetime
import random

class MenuActions(object):
    def __init__(self,  stdout, stderr):

        self.stdout = stdout
        self.stderr = stderr
        self.model_creator = ModelCreator()
        self.classes = Class.objects.all()
        self.students = StudentUser.objects.all()
        self.managers = ManagerUser.objects.filter(is_manager=True)
        self.teachers = TeacherUser.objects.filter(is_manager=False)
        self.schools = School.objects.all()
        self.activities = Activity.objects.all()

        self.activity_types = dict([
            (1, 'Sport'),
            (2, 'Bible'),
            (3, 'Dance'),
            (4, 'Math'),
            (5, 'Literature'),
            (6, 'Geography'),
        ])

        self.main_menu_actions_dict = {
            1: self.create_user_menu_actions,
            2: self.create_class,
            3: self.create_school,
            4: self.create_activitiy,
            5: self.select_one_user_from_menu,
            6: self.show_list_of_classes,
            7: self.generate_activities_data,
            8: self.exit

        }
        self.users_menu_actions_dict = {
            1: self.create_student_user,
            2: self.create_manager_user,
            3: self.create_teacher_user,
            4: self.main_menu_actions,
            5: self.exit
        }

        self.show_users_by_type_actions_dict = {
            1: self.select_student_users_from_menu,
            2: self.select_teachers_users_from_menu,
            3: self.select_managers_users_from_menu
        }



    def main_menu_actions(self, ):
        while (True):
            self.stdout.write("Welcome, \n")
            self.stdout.write("Please choose the following command:")
            self.stdout.write("1. Create user")
            self.stdout.write("2. Create class")
            self.stdout.write("3. Create school")
            self.stdout.write("4. Create activity")
            self.stdout.write("5. Attach class to user")
            self.stdout.write("6. Attach school to class")
            self.stdout.write("7. Generate activities")
            self.stdout.write("8. Exit")
            choice = int(raw_input(">>  "))
            try:
                self.main_menu_actions_dict[choice]()
            except KeyError as e:
                self.stderr.write(e.message)
                continue



    def create_user_menu_actions(self):
        self.stdout.write("Please choose type of user: \n")
        self.stdout.write("1. Student User")
        self.stdout.write("2. Manager User")
        self.stdout.write("3. Teacher User")
        self.stdout.write("4. Back")
        self.stdout.write("5. Exit")
        choice = int(raw_input(">>  "))
        try:
            self.users_menu_actions_dict[choice]()
        except KeyError as e:
            self.stderr.write(e.message)



    def create_user(self):
        auth_user = {}
        user = {}

        self.stdout.write("Please fill the following user fields: \n")
        self.stdout.write("Enter first name: ")
        auth_user['first_name'] = str(raw_input(">>  "))
        self.stdout.write("Enter last name: ")
        auth_user['last_name'] = str(raw_input(">>  "))
        self.stdout.write("Enter email address: ")
        auth_user['email'] = str(raw_input(">>  "))
        self.stdout.write("Enter username: ")
        auth_user['username'] = str(raw_input(">>  "))
        self.stdout.write("Enter password: ")
        auth_user['password'] = str(raw_input(">>  "))
        auth_user['is_staff'] = True
        auth_user['is_active'] = True
        self.stdout.write("Enter Birth date on format %d/%m/%Y (default is None):")
        birth_date = str(raw_input(">>  "))
        try:
            user['birth_date'] = datetime.datetime.strptime(birth_date, '%d/%m/%Y')
        except:
            self.stderr.write("not suppoert format None was setted")
            user['birth_date'] = None

        self.stdout.write("Enter cellphone number: (default is empty): ")
        user['cellphone'] = str(raw_input(">>  "))
        self.stdout.write("Enter address (default is empty): ")
        user['address'] = str(raw_input(">>  "))
        self.stdout.write("Enter gender (M/F): ")
        user['gender'] = str(raw_input(">>  "))
        return auth_user, user



    def create_class(self):
        class_obj = {}
        self.stdout.write("Please fill the following class fields: \n")
        self.stdout.write("Enter class name: ")
        class_obj['name'] = str(raw_input(">>  "))
        class_to_save = Class(**class_obj)
        class_to_save.save()
        self.stdout.write("Class Created Successfully")
        self.attach_school_to_class_handler(class_to_save)
        self.classes = Class.objects.all()
        self.schools = School.objects.all()
        return class_to_save


    def create_school(self):
        school_obj = {}
        self.stdout.write("Please fill the following school fields \n")
        self.stdout.write("Enter school name: ")
        school_obj['name'] = str(raw_input(">>  "))
        self.stdout.write("Enter School level: ")
        school_obj['school_level'] = str(raw_input(">> "))
        school_to_save = School(**school_obj)
        school_to_save.save()
        self.schools = School.objects.all()
        self.stdout.write("School Created successfully")
        return school_to_save


    def create_student_user(self):
        auth_user, student_user = self.create_user()
        password_to_hash = auth_user['password']
        del auth_user['password']
        user = User(**auth_user)
        user.set_password(password_to_hash)
        user.save()
        student_user['user'] = user
        student_user = StudentUser(**student_user)
        student_user.save()
        self.stdout.write("StudentUser Created Successfully")
        self.attach_class_or_classes_to_user_handler(student_user)
        self.classes = Class.objects.all()
        self.students = StudentUser.objects.all()
        self.schools = School.objects.all()




    def create_teacher_user(self):
        auth_user, teacher_user = self.create_user()
        password_to_hash = auth_user['password']
        del auth_user['password']
        user = User(**auth_user)
        user.set_password(password_to_hash)
        user.save()
        teacher_user['user'] = user
        teacher_user = TeacherUser(**teacher_user)
        teacher_user.save()
        self.stdout.write("TeacherUser Created Successfully")
        self.attach_class_or_classes_to_user_handler(teacher_user)
        self.classes = Class.objects.all()
        self.teachers = TeacherUser.objects.filter(is_manager=False)
        self.schools = School.objects.all()



    def create_manager_user(self):
        auth_user, manager_user = self.create_user()
        password_to_hash = auth_user['password']
        del auth_user['password']
        user = User(**auth_user)
        user.set_password(password_to_hash)
        user.save()
        manager_user['user'] = user
        manager_user = ManagerUser(**manager_user)
        manager_user.save()
        self.stdout.write("ManagerUser Created Successfully")
        self.attach_class_or_classes_to_user_handler(manager_user)
        self.classes = Class.objects.all()
        self.managers = ManagerUser.objects.filter(is_manager=True)
        self.schools = School.objects.all()


    def attach_one_class_to_student_user(self, user, current_class):
        user.current_class = current_class
        user.save()
        self.stdout.write('one class attached successfully')

    def create_and_attach_one_class_to_student_user(self, user):
        class_object = self.create_class()
        self.attach_one_class_to_student_user(user, class_object)

    def attach_many_classes_to_manager_or_teacher_user(self, user, list_of_classes):
        for class_object in list_of_classes:
            user.teaching_classes.add(class_object)
        user.save()
        self.stdout.write('%s classes attached successfully' % str(len(list_of_classes)))


    def create_and_attach_many_classes_to_manager_or_teacher_user(self, user):
        self.stdout.write("Enter the number of classes you want to create / attach :")
        classes_objects = []
        number_of_classes = int(raw_input('>>  '))
        for number in range(number_of_classes):
            self.stdout.write("class number %s" % (str(number)))
            class_obj = self.create_class()
            classes_objects.append(class_obj)
        self.attach_many_classes_to_manager_or_teacher_user(user, classes_objects)



    def select_classes_handler(self, user):
        all_classes_mapped_to_indexes = {index: class_obj for index, class_obj in enumerate(self.classes)}
        rows_of_classes = '\n'.join(
                ['{index}. {class_name}'.format(index=index + 1, class_name=class_obj.name) for index, class_obj in
                 all_classes_mapped_to_indexes.items()])
        if isinstance(user, TeacherUser) or isinstance(user, ManagerUser):
            self.select_many_classes_from_menu(user, all_classes_mapped_to_indexes, rows_of_classes)
        else:
            self.select_one_class_from_menu(user, all_classes_mapped_to_indexes, rows_of_classes)




    def select_many_classes_from_menu(self, user, all_classes_mapped_to_indexes, rows_of_classes):
        list_of_classes_to_attach = []
        self.stdout.write("Enter the number of classes you want to attach: ")
        number_of_classes_to_attach = int(raw_input('>>  '))
        for number in range(number_of_classes_to_attach):
            os.system('clear')
            self.stdout.write("Please attach classes to user from the following list: \n")
            self.stdout.write(rows_of_classes)
            self.stdout.write("class number %s" % str(number + 1))
            class_index = int(raw_input('>>  ')) - 1
            class_object = all_classes_mapped_to_indexes[class_index]
            list_of_classes_to_attach.append(class_object)
        self.attach_many_classes_to_manager_or_teacher_user(user, list_of_classes_to_attach)



    def select_one_class_from_menu(self, user, all_classes_mapped_to_indexes, rows_of_classes):
        self.stdout.write("Please attach class to user from the following list: \n")
        self.stdout.write(rows_of_classes)
        class_index = int(raw_input('>>  ')) - 1
        class_object = all_classes_mapped_to_indexes[class_index]
        self.attach_one_class_to_student_user(user, class_object)


    def create_class_handler(self, user):
        self.stdout.write("There is no classes to display \n")
        if isinstance(user, TeacherUser) or isinstance(user, ManagerUser):
            self.create_and_attach_many_classes_to_manager_or_teacher_user(user)
        else:
            self.create_and_attach_one_class_to_student_user(user)


    def attach_class_or_classes_to_user_handler(self, user):
        if self.classes:
            self.select_classes_handler(user)
        else:
            self.create_class_handler(user)



    def attach_one_school_to_class(self, current_class, school_object):
        current_class.school = school_object
        current_class.save()



    def create_and_attach_one_school_to_class(self, current_class):
        school = self.create_school()
        self.attach_one_school_to_class(current_class, school)



    def select_one_school_from_menu(self, current_class):
        all_schools_mapped_to_indexes = {index: school for index, school in enumerate(self.schools)}
        rows_of_schools = '\n'.join(
                ['{index}. {school_name}'.format(index=index + 1, school_name=school.name) for index, school in
                 all_schools_mapped_to_indexes.items()])
        self.stdout.write("Please attach school to class from the following list: \n")
        self.stdout.write(rows_of_schools)
        school_index = int(raw_input('>>  ')) - 1
        school_object = all_schools_mapped_to_indexes[school_index]
        self.attach_one_school_to_class(current_class, school_object)




    def attach_school_to_class_handler(self, current_class):

        if self.schools:
            self.select_one_school_from_menu(current_class)
        else:
            self.create_and_attach_one_school_to_class(current_class)



    def select_one_user_from_menu(self):
        self.stdout.write("1. Student User")
        self.stdout.write("2. Teacher User")
        self.stdout.write("3. Manager User")
        choice = int(raw_input('>>  '))
        try:
            self.show_users_by_type_actions_dict[choice]()
        except KeyError as e:
            self.stderr.write(e.message)



    def show_list_of_classes(self):
        all_classes_mapped_to_indexes = {index: class_obj for index, class_obj in enumerate(self.classes)}
        rows_of_classes = '\n'.join(
                ['{index}. {class_name}'.format(index=index + 1, class_name=class_obj.name) for index, class_obj in
                 all_classes_mapped_to_indexes.items()])
        self.stdout.write("Please select class from the following list: \n")
        self.stdout.write(rows_of_classes)
        class_index = int(raw_input('>>  ')) - 1
        class_object = all_classes_mapped_to_indexes[class_index]
        self.attach_school_to_class_handler(class_object)



    def select_teachers_users_from_menu(self):
        all_teachers_students_mapped_to_indexes = {index: teacher_user for index, teacher_user in enumerate(self.teachers)}
        rows_of_teacher_users = '\n'.join(
                ['{index}. {teacher_name}'.format(index=index+1, teacher_name=teacher.user.get_full_name()) for index, teacher in
                 all_teachers_students_mapped_to_indexes.items()])
        self.stdout.write("Please select teacher from the following list: \n")
        self.stdout.write(rows_of_teacher_users)
        teacher_index = int(raw_input('>>  ')) - 1
        teacher_object = all_teachers_students_mapped_to_indexes[teacher_index]
        self.attach_class_or_classes_to_user_handler(teacher_object)



    def select_managers_users_from_menu(self):
        all_managers_users_mapped_to_indexes = {index: manager_user for index, manager_user in enumerate(self.managers)}
        rows_of_manager_users = '\n'.join(
                ['{index}. {manager_name}'.format(index=index+1, manager_name=manager.user.get_full_name()) for index, manager in
                 all_managers_users_mapped_to_indexes.items()])
        self.stdout.write("Please select manager from the following list: \n")
        self.stdout.write(rows_of_manager_users)
        manager_index = int(raw_input('>>  ')) - 1
        manager_object = all_managers_users_mapped_to_indexes[manager_index]
        self.attach_class_or_classes_to_user_handler(manager_object)



    def select_student_users_from_menu(self):
        all_student_users_mapped_to_indexes = {index: student_user for index, student_user in enumerate(self.students)}
        rows_of_student_users = '\n'.join(
                ['{index}. {student_name}'.format(index=index+1, student_name=student.user.get_full_name()) for index, student in
                 all_student_users_mapped_to_indexes.items()])
        self.stdout.write("Please select student from the following list: \n")
        self.stdout.write(rows_of_student_users)
        student_index = int(raw_input('>>  ')) - 1
        student_object= all_student_users_mapped_to_indexes[student_index]
        self.attach_class_or_classes_to_user_handler(student_object)


    def create_activitiy(self):
        activity_obj = {}
        self.stdout.write("Please fill the following activity fields: \n")
        self.stdout.write("Choose activity type from the following: ")
        activities_type_rows = '\n'.join(["{index}. {activity_type}".format(index=index, activity_type=activity_type)
                                          for index, activity_type in self.activity_types.items()])
        self.stdout.write(activities_type_rows)
        choice = int(raw_input(">>  "))
        activity_obj['activity_type'] = self.activity_types[choice]
        self.stdout.write("Please set short description (default is '')")
        activity_obj['description'] = str(raw_input(">>  "))
        activity_to_save = Activity(**activity_obj)
        activity_to_save.save()
        self.stdout.write("Activity Created Successfully")
        self.activities = Activity.objects.all()
        return activity_to_save


    def generate_activities_data(self):
        if not self.activities:
            self.stderr.write("you need to create activities first")
        elif not self.students:
            self.stderr.write("print you need to create students first")
        else:
            self.stdout.write("Enter the number of activities data to generate per student")
            generate_number = int(raw_input(">>  "))
            most_students, rest_of_students = self.split_students()
            for student in most_students:
                for index in range(generate_number):
                    random_activity = self.select_random_activity()
                    if random_activity:
                        self.create_activity_data(random_activity, student, generate_number, 80)
                    else:
                        self.stderr.write("no activities inserted")
                        return
            for student_index, student in enumerate(rest_of_students):
                for index in range(generate_number):
                    random_activity = self.select_random_activity()
                    if random_activity:
                        mu = 90 if index % 2 == 0 else 60
                        self.create_activity_data(random_activity, student, generate_number, mu)
                    else:
                        self.stderr.write("no activities inserted")
                        return



    def split_students(self):
        num_of_students = self.students.count()
        if num_of_students > 0:
            if num_of_students == 1:
                return self.students, []
            most = int(round(2 / 3 * num_of_students))
            return self.students[0: most], self.students[most:]
        else:
            self.stderr.write("no data generated please enter students")
            return [], []


    def select_random_activity(self):
        if self.activities.count() > 0:
            random_index = random.randint(0, self.activities.count() -1)
            return self.activities[random_index]
        else:
            return None


    def create_activity_data(self, random_activity, student, generate_number, mu):
        activity_data = ActivityData()
        activity_data.student = student
        activity_data.activity = random_activity
        activity_data.score = self.generate_score(generate_number, mu)
        activity_data.date = datetime.datetime.now()
        activity_data.save()

    def generate_score(self, generate_number, mu):
        print 'gal'
        number = round(random.gauss(mu, generate_number))
        return number if number < 100 else 100



    def exit(self):
        sys.exit()
