from school_management_system_app.models import *

class ModelCreator(object):
    def __init__(self):
        self.models_dict = {
            'StudentUser': StudentUser,
            'TeacherUser': TeacherUser,
            'ManagerUser': ManagerUser,
            'Class': Class,
            'Activity': Activity,
            'ActivityData': ActivityData

        }
    def create(self, model_name=None,  *args, **kwargs):
        if model_name is None:
            raise Exception('model name param must be provided')
        Model = self.models_dict[model_name]
        model = Model(args, kwargs)
        model.save()


    def attach_related_elements(self, model_name=None, model_pk=None, related_field_model_name=None, related_field_name=None, related_field_pks=[], many=False):
        if related_field_name is None or many is None or model_name is None:
            raise Exception("model_pk / model_name / related_field_name / many params must be provided")
        Model = self.models_dict[model_name]
        current_model = Model.objects.get(pk=model_pk)
        RelatedModel = self.models_dict[related_field_model_name]
        if many:
            related_field_elements_to_add = RelatedModel.objects.filter(pk__in=related_field_pks)
            for related_element_to_add in related_field_elements_to_add:
                related_field_many = getattr(current_model, related_field_name)
                related_field_add_function = getattr(related_field_many, 'add')
                related_field_add_function(related_element_to_add)
        else:
            related_field_element = RelatedModel.objects.get(pk=related_field_pks)
            setattr(current_model, related_field_name, related_field_element)
        current_model.save()




