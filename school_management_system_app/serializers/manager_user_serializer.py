from rest_framework import serializers
from school_management_system.settings import REST_FRAMEWORK_USER_EXCLUDED_FIELDS
from school_management_system_app.models import ManagerUser, BaseUser
from school_management_system_app.serializers.class_serializer import ClassSerializer
from school_management_system_app.serializers.user_serializer import UserSerializer
from school_management_system_app.serializers.depth_school_serializer import SchoolDepthSerializer

class ManagerUserSeralizer(serializers.ModelSerializer):
    user = UserSerializer()
    teaching_classes = ClassSerializer(many=True)
    get_user_tag = serializers.ReadOnlyField()
    school = SchoolDepthSerializer()

    class Meta:
        model = ManagerUser
        fields = tuple(
            field.name for field in BaseUser._meta.fields if field.name not in REST_FRAMEWORK_USER_EXCLUDED_FIELDS) + (
                     'teaching_classes', 'user', 'get_user_tag', 'is_manager', 'school')
