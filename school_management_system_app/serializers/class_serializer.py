from rest_framework import serializers
from school_management_system_app.models import Class
from school_management_system_app.serializers.school_serializer import SchoolSerializer
from school_management_system_app.serializers.student_user_serializer import StudentUserSerializer


class ClassSerializer(serializers.ModelSerializer):
    school = SchoolSerializer()
    get_average = serializers.ReadOnlyField()
    class_students = StudentUserSerializer(many=True)

    class Meta:
        model = Class
        fields = tuple(
            field.name for field in Class._meta.fields) + ('get_average', 'class_students', 'school')

