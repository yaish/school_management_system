from rest_framework import serializers
from school_management_system_app.models import StudentUser, BaseUser
from school_management_system_app.serializers.activity_data_serializer import ActivityDataSerizlier
from school_management_system.settings import REST_FRAMEWORK_USER_EXCLUDED_FIELDS
from school_management_system_app.serializers.short_class_serializer import ShortClassSerializer
from school_management_system_app.serializers.user_serializer import UserSerializer


class StudentUserSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    student_activity_data = ActivityDataSerizlier(many=True)
    current_class = ShortClassSerializer()
    get_average = serializers.ReadOnlyField()
    get_user_tag = serializers.ReadOnlyField()


    class Meta:
        model = StudentUser
        fields = tuple(
            field.name for field in BaseUser._meta.fields if field.name not in REST_FRAMEWORK_USER_EXCLUDED_FIELDS) + (
                     'student_activity_data', 'current_class', 'get_average', 'user', 'get_user_tag')
