from rest_framework import serializers
from school_management_system_app.models import School
from school_management_system_app.serializers.class_serializer import ClassSerializer


class SchoolDepthSerializer(serializers.ModelSerializer):
    get_average = serializers.ReadOnlyField()
    school_classes = ClassSerializer(many=True)

    class Meta:
        model = School
        fields = tuple(
            field.name for field in School._meta.fields) + ('school_classes', 'get_average', )
