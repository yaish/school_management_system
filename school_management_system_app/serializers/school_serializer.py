from rest_framework import serializers
from school_management_system_app.models import School


class SchoolSerializer(serializers.ModelSerializer):
    get_average = serializers.ReadOnlyField()

    class Meta:
        model = School
        fields = tuple(
            field.name for field in School._meta.fields) + ('get_average', )
