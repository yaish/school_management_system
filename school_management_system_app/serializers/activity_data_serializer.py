from rest_framework import serializers
from school_management_system_app.models import ActivityData
from school_management_system_app.serializers.activity_serializer import ActivitySerializer

class ActivityDataSerizlier(serializers.ModelSerializer):
    activity = ActivitySerializer()

    class Meta:
        model = ActivityData