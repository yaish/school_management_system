from rest_framework import serializers
from school_management_system_app.models import Class
from school_management_system_app.serializers.school_serializer import SchoolSerializer


class ShortClassSerializer(serializers.ModelSerializer):
    school = SchoolSerializer()
    get_average = serializers.ReadOnlyField()
    class Meta:
        model = Class
        fields = tuple(
            field.name for field in Class._meta.fields) + ('get_average', 'school', )