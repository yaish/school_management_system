from rest_framework import serializers
from school_management_system.settings import REST_FRAMEWORK_USER_EXCLUDED_FIELDS
from school_management_system_app.models import TeacherUser, BaseUser
from school_management_system_app.serializers.class_serializer import ClassSerializer
from school_management_system_app.serializers.user_serializer import UserSerializer


class TeacherUserSeralizer(serializers.ModelSerializer):
    user = UserSerializer()
    teaching_classes = ClassSerializer(many=True)
    get_user_tag = serializers.ReadOnlyField()

    class Meta:
        model = TeacherUser
        fields = tuple(
            field.name for field in BaseUser._meta.fields if field.name not in REST_FRAMEWORK_USER_EXCLUDED_FIELDS) + (
                     'teaching_classes', 'user', 'get_user_tag', 'is_manager')
