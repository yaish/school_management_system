from rest_framework import serializers
from school_management_system_app.models import Activity


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
