from django.contrib import admin
from .models import *
from django.contrib.auth.models import User
admin.site.register(StudentUser)
admin.site.register(TeacherUser)
admin.site.register(ManagerUser)
admin.site.register(Class)
admin.site.register(Activity)
admin.site.register(ActivityData)
admin.site.register(School)


