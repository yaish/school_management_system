# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('activity_type', models.CharField(default=b'Sport', max_length=256, choices=[(b'Sport', b'Sport'), (b'Bible', b'Bible'), (b'Dance', b'Dance'), (b'Math', b'Math'), (b'Litreture', b'Litreture'), (b'Geography', b'Geography')])),
                ('description', models.TextField(default=b'', max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='ActivityData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(null=True, blank=True)),
                ('score', models.IntegerField(default=0)),
                ('activity', models.ForeignKey(related_name='activity_data', blank=True, to='school_management_system_app.Activity', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='BaseUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('birth_date', models.DateField(null=True, blank=True)),
                ('telephone', models.CharField(default=b'', max_length=256)),
                ('cellphone', models.CharField(default=b'', max_length=256)),
                ('address', models.CharField(default=b'', max_length=256)),
                ('gender', models.CharField(max_length=1, choices=[(b'M', b'Male'), (b'F', b'Female')])),
            ],
        ),
        migrations.CreateModel(
            name='Class',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='School',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=256)),
                ('school_level', models.IntegerField(default=1, choices=[(1, b'EASY'), (2, b'MEDIUM'), (3, b'HARD')])),
            ],
        ),
        migrations.CreateModel(
            name='StudentUser',
            fields=[
                ('baseuser_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='school_management_system_app.BaseUser')),
                ('activities', models.ManyToManyField(to='school_management_system_app.Activity', null=True, through='school_management_system_app.ActivityData', blank=True)),
            ],
            options={
                'verbose_name': 'StudentUser',
            },
            bases=('school_management_system_app.baseuser',),
        ),
        migrations.CreateModel(
            name='TeacherUser',
            fields=[
                ('baseuser_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='school_management_system_app.BaseUser')),
            ],
            options={
                'verbose_name': 'TeacherUser',
            },
            bases=('school_management_system_app.baseuser',),
        ),
        migrations.AddField(
            model_name='class',
            name='school',
            field=models.ForeignKey(related_name='school_classes', blank=True, to='school_management_system_app.School', null=True),
        ),
        migrations.AddField(
            model_name='baseuser',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.CreateModel(
            name='ManagerUser',
            fields=[
                ('teacheruser_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='school_management_system_app.TeacherUser')),
            ],
            options={
                'verbose_name': 'ManagerUser',
            },
            bases=('school_management_system_app.teacheruser',),
        ),
        migrations.AddField(
            model_name='teacheruser',
            name='teaching_classes',
            field=models.ManyToManyField(related_name='class_teachers', null=True, to='school_management_system_app.Class', blank=True),
        ),
        migrations.AddField(
            model_name='studentuser',
            name='current_class',
            field=models.ForeignKey(related_name='class_students', blank=True, to='school_management_system_app.Class', null=True),
        ),
        migrations.AddField(
            model_name='activitydata',
            name='student',
            field=models.ForeignKey(related_name='student_activity_data', blank=True, to='school_management_system_app.StudentUser', null=True),
        ),
    ]
