# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school_management_system_app', '0002_auto_20161217_2003'),
    ]

    operations = [
        migrations.AddField(
            model_name='manageruser',
            name='school',
            field=models.ForeignKey(blank=True, to='school_management_system_app.School', null=True),
        ),
    ]
