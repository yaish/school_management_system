# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('school_management_system_app', '0004_auto_20161219_2022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='activity_type',
            field=models.CharField(default=b'Sport', max_length=256, choices=[(b'Sport', b'Sport'), (b'Bible', b'Bible'), (b'Dance', b'Dance'), (b'Math', b'Math'), (b'Literature', b'Literature'), (b'Geography', b'Geography')]),
        ),
    ]
