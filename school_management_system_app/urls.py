from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework_extensions.routers import ExtendedDefaultRouter

from school_management_system.views import IndexView
from school_management_system_app.viewsets.activity_data_viewset import ActivityDataViewSet
from school_management_system_app.viewsets.activity_viewset import ActivityViewSet
from school_management_system_app.viewsets.class_viewset import ClassViewSet
from school_management_system_app.viewsets.current_user_viewset import CurrentUserViewSet
from school_management_system_app.viewsets.get_all_weak_students_apiview import WeakStudentsApiView
from school_management_system_app.viewsets.manager_user_viewset import ManagerUserViewSet
from school_management_system_app.viewsets.student_user_viewset import StudentUserViewSet
from school_management_system_app.viewsets.teacher_user_viewset import TeacherUserViewSet
from school_management_system_app.viewsets.school_viewset import SchoolViewSet

admin.autodiscover()

router = ExtendedDefaultRouter()
(
    router.register('students-users', StudentUserViewSet, base_name='students-users')
        .register('students-activity-data', ActivityDataViewSet, base_name='students-activity-data',
                  parents_query_lookups=['student']),

    router.register('activities', ActivityViewSet, base_name='activities')
        .register('activity-data', ActivityDataViewSet, base_name='activity-data', parents_query_lookups=['activity']),

    router.register('classes', ClassViewSet, base_name='classes')
        .register('students-users', StudentUserViewSet, base_name='students-users',
                  parents_query_lookups=['current_class']),

    router.register('classes', ClassViewSet, base_name='classes')
        .register('teachers-users', TeacherUserViewSet, base_name='teachers-users',
                  parents_query_lookups=['teaching_classes']),

    router.register('teachers-users', TeacherUserViewSet, base_name='teachers-users')
        .register('classes', ClassViewSet, base_name='classes', parents_query_lookups=['class_teachers']),

    router.register('classes', ClassViewSet, base_name='classes')
        .register('managers-users', ManagerUserViewSet, base_name='managers-users',
                  parents_query_lookups=['teaching_classes']),

    router.register('managers-users', ManagerUserViewSet, base_name='managers-users')
        .register('classes', ClassViewSet, base_name='classes', parents_query_lookups=['class_teachers']),

    router.register('schools', SchoolViewSet, base_name='schools')
)

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'', include(router.urls)),
                       url(r'^view/', IndexView.as_view(), name='index'),
                       url(r'^me/', CurrentUserViewSet.as_view(), name='me'),
                       url(r'^get_all_weak_students', WeakStudentsApiView.as_view(), name='get_all_weak_students')

                       )
