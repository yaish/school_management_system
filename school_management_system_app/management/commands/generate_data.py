import sys
from django.core.management.base import BaseCommand
from school_management_system_app.utils.menu_actions import MenuActions

class Command(BaseCommand):

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)


    def handle(self, *args, **options):
            menu_actions = MenuActions(self.stdout, self.stderr)
            menu_actions.main_menu_actions()

