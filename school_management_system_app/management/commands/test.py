from django.core.management.base import BaseCommand
import sys
CREATE_USER = 'create_user'
CREATE_CLASS = 'create_class'

class Command(BaseCommand):
    help = 'Used to generate all db relations and users'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.stdin = sys.stdin

    def add_arguments(self, parser):
        parser.add_argument(
            '--%s' % CREATE_USER,
            dest=CREATE_USER, default=None,
            help='create student/teacher users.',
        )

        parser.add_argument(
            '--%s' % CREATE_CLASS,
            dest=CREATE_CLASS, default=None,
            help='create class',
        )

    def handle(self, *args, **options):
        self.stdout.write(' >> ')
        number = self.stdin.readline()
        print number

