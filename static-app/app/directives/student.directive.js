

(function () {
    'use strict';
    angular
        .module('smsApp')
        .directive('studentRow', studentRow);
    studentRow.$inject = ['appService', '$timeout', '$state', '$compile'];

    function studentRow(appService, $timeout, $state) {
        return {

            template: '<tr>' +
            '<td ng-bind="student.user.first_name"></td>' +
            '<td ng-bind="student.get_averge "></td>' +
            '<td ng-bind="student.get_average"></td>' +
            '<td><button ng-click="showStudentActivities(student)" class="btn btn-warning">show activities</button></td>' +
            '</tr>',

            link: function ($scope) {
                console.log('student');
                $scope.showStudentActivities = function (student) {
                    appService.setStudent(student);
                    $timeout(function () {
                        $state.go('app.activities');
                    }, 0);
                }
            }
        }
    }
})();
