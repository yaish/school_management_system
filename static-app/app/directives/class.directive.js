(function () {
    'use strict';
    angular
        .module('smsApp')
        .directive('classObject', classObject);

    classObject.$inject = ['appService', '$timeout', '$state'];

    function classObject(appService, $timeout, $state) {
        return {
            restrict: "E",
            replace: true,
            scope: {
                classObject: '=',
                schoolScore: '='
            },
            templateUrl: '/static-app/app/directives/class.html',

            link: function ($scope) {
                $scope.showStudentActivities = function (student) {
                    appService.setStudent(student);
                    $timeout(function () {
                        $state.go('app.activities');
                    }, 0);
                }
            }
        }
    }
})();