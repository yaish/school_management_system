(function () {
    'use strict';

    angular
        .module('smsApp')
        .factory('appService', appService);

    appService.$inject = ['$q', '$http', 'tokenManager'];


    /* @ngInject */
    function appService($q, $http, tokenManager) {
        var service = {
            user: {},
            student: {},
            setStudent: setStudent,
            getStudent: getStudent,
            setUser: setUser,
            getUser: getUser,
            login: login,
            logout: logout,
            setConnectedUserFromApi: setConnectedUserFromApi
        };

        return service;

        ////////////////

        function setUser(user) {
            service.user = user;
        }

        function getUser() {
            return service.user
        }

        function setStudent(student){
            service.student = student
        }

        function getStudent(){
            return service.student;
        }


        function login(username, password, callback) {
            return $http.post('/auth/login/', {username: username, password: password})
                .then(function(data, status, header, config){
                    loginSuccess(data, status, header, config);
                    callback(true);
                })
                .catch(function (message) {
                    callback(false);
                })
        }

        function logout() {
            $http.post('/auth/logout/')
                .then(function(data, status, header, config){
                    logoutSuccess(data, status, header, config)
                })
                .catch(function (message) {
                    console.error(message);
                })
        }

        function loginSuccess(data, status, header, config) {
            tokenManager.setToken(data.data.auth_token);

        }

        function logoutSuccess(data, status, header, config) {
            tokenManager.setToken();
            setUser({})
        }

        function setConnectedUserFromApi(callback) {
            if (tokenManager.getToken()) {
                $http.get('/api/v1/sms-app/me/').then(function (resp) {
                    setUser(resp.data);
                    callback(resp.data)
                });
            }
            else
                return $q.reject({message: 'User has no token.'});
        }


    }
})();