(function () {
    'use strict';

    angular
        .module('smsApp')
        .controller('performanceBoxController', performanceBoxController);

    performanceBoxController.$inject = ['appService', '$timeout', '$state'];


    function performanceBoxController(appService, $timeout, $state) {

        var vm = this;
        vm.activate = activate;
        vm.currentUser = appService.getUser();
        vm.school = {};
        vm.schoolDisplayName = '';
        vm.schoolScore = 0;
        vm.isManager = false;
        vm.isTeacher = false;
        vm.isStudent = false;
        vm.filterByDate = false;
        vm.filterByClass = false;
        vm.chosenClass = {};
        vm.chosenFromDate = '';
        vm.chosenToDate = '';
        vm.classIndexToStudentsList = {};
        vm.allClasses = [];
        vm.allClassesViewOnly = [];
        vm.calcDiff = calcDiff;
        vm.checkTypeOfUser = checkTypeOfUser;
        vm.setSchool = setSchool;
        vm.setClasses = setClasses;
        vm.filterClassesByChosenClass = filterClassesByChosenClass;
        vm.filterClassStudentsByDateRange = filterClassStudentsByDateRange;
        vm.clearAll = clearAll;
        vm.filterAll = filterAll;
        vm.saveStudentsPerClassCopy = saveStudentsPerClassCopy;
        vm.retrieveSavedStudents = retrieveSavedStudents;
        vm.showStudentActivities = showStudentActivities;
        vm.setSchoolByManager = setSchoolByManager;
        vm.setSchoolByTeacher = setSchoolByTeacher;

        activate();


        //////////////////////////////

        function activate() {
            checkTypeOfUser();
            setClasses();
            setSchool();
            saveStudentsPerClassCopy();
            vm.schoolDisplayName = vm.school.name + ' ' + 'Performance';
            vm.schoolScore = vm.school.average;
        }


        function clearAll() {
            vm.filterByDate = false;
            vm.filterByClass = false;
            vm.chosenClass = {};
            vm.chosenFromDate = "";
            vm.chosenToDate = "";
            setClasses();
            retrieveSavedStudents()
        }


        function filterAll() {
            setClasses();
            retrieveSavedStudents();
            if (vm.filterByClass) {
                if (Object.keys(vm.chosenClass).length > 0)
                    filterClassesByChosenClass();
            }

            if (vm.filterByDate) {
                if (vm.chosenFromDate != '' && vm.chosenToDate != '') {
                    filterClassStudentsByDateRange();
                }
            }
        }

        function filterClassesByChosenClass() {
            vm.allClasses = vm.allClasses.filter(function (ele) {
                return ele.id == vm.chosenClass.id
            })
        }

        function filterClassStudentsByDateRange() {
            var fromDate = new Date(vm.chosenFromDate);
            var toDate = new Date(vm.chosenToDate);
            for (var i = 0; i < vm.allClasses.length; i++) {
                vm.allClasses[i].class_students = filterStudentsByActivitiesRange(fromDate, toDate, vm.allClasses[i].class_students)
            }
        }

        function filterStudentsByActivitiesRange(fromDate, toDate, currentStudents) {
            return currentStudents.filter(function (student) {
                var filteredActivites = filterActivitiesByDateRange(fromDate, toDate, student.student_activity_data);
                return filteredActivites.length > 0;

            })
        }

        function filterActivitiesByDateRange(fromDate, toDate, activities) {
            return activities.filter(function (activity) {
                var activityDate = new Date(activity.date.split('T')[0]);
                return activityDate >= fromDate && activityDate <= toDate
            })
        }

        function calcDiff(student) {
            $scope.Math = Math;
            return $scope.Math.abs(student.get_average - vm.school.get_average)
        }

        function checkTypeOfUser() {
            if (vm.currentUser.get_user_tag == "T") {
                vm.isTeacher = true;
            }
            else {
                vm.isManager = true;
            }
        }


        function saveStudentsPerClassCopy() {
            for (var index = 0; index < vm.allClasses.length; index++) {
                vm.classIndexToStudentsList[index] = vm.allClasses[index].class_students.slice();
            }
        }

        function retrieveSavedStudents() {
            for (var index = 0; index < vm.allClasses.length; index++) {
                vm.allClasses[index].class_students = vm.classIndexToStudentsList[index]
            }
        }

        function showStudentActivities(student) {
            appService.setStudent(student);
            $timeout(function () {
                $state.go('app.activities');
            }, 0);
        }

        function setClasses() {
            if (vm.isManager) {
                vm.allClasses = vm.currentUser.school.school_classes;
            }
            else if (vm.isTeacher) {
                vm.allClasses = vm.currentUser.teaching_classes;
            }

            vm.allClassesViewOnly = vm.allClasses.slice()
        }


        function setSchool() {
            if (vm.isManager) {
                setSchoolByManager();
            }
            else if (vm.isTeacher) {
                setSchoolByTeacher()
            }

        }


        function setSchoolByTeacher() {
            if (vm.currentUser.teaching_classes.length > 0) {
                vm.school = {
                    name: vm.currentUser.teaching_classes[0].school.name,
                    average: vm.currentUser.teaching_classes[0].school.get_average
                }
            }
            else {
                vm.school = {
                    name: "",
                    average: 0
                }
            }
        }

        function setSchoolByManager() {
            vm.school = {
                name: vm.currentUser.school.name,
                average: vm.currentUser.school.get_average
            }
        }


    }
})();