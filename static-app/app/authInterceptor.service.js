(function(){
    'use strict';

    angular
        .module('smsApp')
        .factory('authInterceptor' , authInterceptor);

    authInterceptor.$inject = ['$q', '$location', 'tokenManager'];

    /* @ngInject */

    function authInterceptor($q, $location, tokenManager)
    {
        var service = {
            request : request,
            responseError : responseError
        };

        return service;

        ///////////////

        function request(config){
            var token = tokenManager.getToken();
            if(token)
                config.headers['Authorization'] = 'Token ' + token;
            return config;
        }


        function responseError(response){
            if(response.status == 403){
                tokenManager.setToken();
                $location.path('/');
            }

            return $q.reject(response);
        }
    }
})();