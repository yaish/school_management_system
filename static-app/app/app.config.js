/**
 * Created by gal on 17/12/2016.
 */
(function () {
    'use strict';

    angular
        .module('smsApp')
        .run(run)
        .config(config);

    run.$inject = ['$http'];
    config.$inject = ['$locationProvider', '$httpProvider'];

    function run($http){
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
        $http.defaults.xsrfCookieName = 'csrftoken';
    }

    function config($locationProvider, $httpProvider){
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
        $httpProvider.interceptors.push('authInterceptor');
    }
})();