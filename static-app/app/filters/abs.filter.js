(function () {
    'use strict';

    angular
        .module('smsApp')
        .filter('absFilter', absFilter);

    function absFilter() {
        return function (val) {
            return (Math.abs(val)).toFixed(1)
        }
    }
})();