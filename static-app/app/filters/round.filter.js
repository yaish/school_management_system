(function () {
    'use strict';

    angular
        .module('smsApp')
        .filter('roundFilter', roundFilter);

    function roundFilter() {
        return function (val) {
            return val.toFixed(1)
        }
    }
})();