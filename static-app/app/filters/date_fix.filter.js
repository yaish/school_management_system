(function () {
    'use strict';

    angular
        .module('smsApp')
        .filter('dateFixFilter', dateFixFilter);

    function dateFixFilter() {
        return function (date) {
            return date.split('T')[0]
        }
    }
})();