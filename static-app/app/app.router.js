(function () {
    'use strict';
    angular
        .module('smsApp')
        .run(run)
        .config(config);

    run.$inject = ['$rootScope', '$state', '$stateParams'];
    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function run($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }

    function config($stateProvider, $urlRouterProvider) {
        $urlRouterProvider
            .otherwise('/api/v1/sms-app/view/');

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: '/static-app/app/authentication/login.html'
            })

            .state('app', {
                url: '/app',
                templateUrl: '/static-app/app/navbar/navbar.html'
            })

            .state('app.graph', {
                url: '/graph',
                templateUrl: '/static-app/app/graph/graph.html'
            })

            .state('app.performance_box', {
                url: '/performance-box',
                templateUrl: '/static-app/app/performance_box/performance_box.html'
            })
            .state('app.weak_students', {
                url: '/weak-students',
                templateUrl: '/static-app/app/weak_students/weak_students.html'
            })
            .state('app.activities', {
                url: '/activities',
                templateUrl: '/static-app/app/activities/activities.html'
            })
    }


})();