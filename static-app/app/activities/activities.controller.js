/**
 * Created by galyaish on 20/12/2016.
 */
(function () {
    'use strict';

    angular
        .module('smsApp')
        .controller('activitiesController', activitiesController);

    activitiesController.$inject = ['appService'];


    function activitiesController(appService) {

        var vm = this;
        vm.currentStudent = appService.getStudent();
        vm.headline = 'List of ' + vm.currentStudent.user.first_name + ' ' + vm.currentStudent.user.last_name + ' activities'
        vm.activate = activate;

        activate();


        //////////////////////////////


        function activate() {
        }

    }
})();