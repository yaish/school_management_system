(function () {
    'use strict';

    angular
        .module('smsApp')
        .controller('graphController', graphController);

    graphController.$inject = ['appService'];


    function graphController(appService) {

        var vm = this;
        vm.chart = {};
        vm.chartOptions = {};
        vm.chartData = {};
        vm.currentUser = appService.getUser();
        vm.isManager = false;
        vm.isTeacher = false;
        vm.isStudent = false;
        vm.filterByDate = false;
        vm.filterByClass = false;
        vm.chosenClass = {};
        vm.chosenFromDate = '';
        vm.chosenToDate = '';
        vm.classIndexToStudentsList = {};
        vm.studentsNames = [];
        vm.studentsScores = [];
        vm.allClasses = [];
        vm.allClassesViewOnly = [];
        vm.activate = activate;
        vm.setStudentScores = setStudentScores;
        vm.setChartOptions = setChartOptions;
        vm.initChart = initChart;
        vm.displayChart = displayChart;
        vm.setClasses = setClasses;
        vm.checkTypeOfUser = checkTypeOfUser;
        vm.filterClassesByChosenClass = filterClassesByChosenClass;
        vm.filterClassStudentsByDateRange = filterClassStudentsByDateRange;
        vm.clearAll = clearAll;
        vm.filterAll = filterAll;
        vm.saveStudentsPerClassCopy = saveStudentsPerClassCopy;
        vm.retrieveSavedStudents = retrieveSavedStudents;

        activate();


        //////////////////////////////

        function activate() {
            checkTypeOfUser();
            setClasses();
            saveStudentsPerClassCopy();
            setStudentScores();
            setChartOptions();
            initChart();
            displayChart();
        }

        function clearAll() {
            vm.filterByDate = false;
            vm.filterByClass = false;
            vm.chosenClass = {};
            vm.chosenFromDate = "";
            vm.chosenToDate = "";
            vm.studentsNames = [];
            vm.studentsScores = [];
            vm.chartOptions = {};
            vm.chartData = {};
            vm.chart.destroy();
            setClasses();
            retrieveSavedStudents();
            setStudentScores();
            setChartOptions();
            initChart();
            displayChart();
        }


        function filterAll() {
            setClasses();
            retrieveSavedStudents();
            if (vm.filterByClass) {
                if (Object.keys(vm.chosenClass).length > 0)
                    filterClassesByChosenClass();
            }

            if (vm.filterByDate) {
                if (vm.chosenFromDate != '' && vm.chosenToDate != '') {
                    filterClassStudentsByDateRange();
                }
            }
            vm.studentsNames = [];
            vm.studentsScores = [];
            vm.chart.destroy();
            setStudentScores();
            setChartOptions();
            initChart();
            displayChart();
        }

        function filterClassesByChosenClass() {
            vm.allClasses = vm.allClasses.filter(function (ele) {
                return ele.id == vm.chosenClass.id
            })
        }

        function filterClassStudentsByDateRange() {
            var fromDate = new Date(vm.chosenFromDate);
            var toDate = new Date(vm.chosenToDate);
            for (var i = 0; i < vm.allClasses.length; i++) {
                vm.allClasses[i].class_students = filterStudentsByActivitiesRange(fromDate, toDate, vm.allClasses[i].class_students)
            }
        }

        function filterStudentsByActivitiesRange(fromDate, toDate, currentStudents) {
            return currentStudents.filter(function (student) {
                var filteredActivites = filterActivitiesByDateRange(fromDate, toDate, student.student_activity_data);
                return filteredActivites.length > 0;

            })
        }

        function filterActivitiesByDateRange(fromDate, toDate, activities) {
            return activities.filter(function (activity) {
                var activityDate = new Date(activity.date.split('T')[0]);
                return activityDate >= fromDate && activityDate <= toDate
            })
        }

        function checkTypeOfUser() {
            if (vm.currentUser.get_user_tag == "T") {
                vm.isTeacher = true;
            }
            else {
                vm.isManager = true;
            }
        }

        function saveStudentsPerClassCopy() {
            for (var index = 0; index < vm.allClasses.length; index++) {
                vm.classIndexToStudentsList[index] = vm.allClasses[index].class_students.slice();
            }
        }

        function retrieveSavedStudents() {
            for (var index = 0; index < vm.allClasses.length; index++) {
                vm.allClasses[index].class_students = vm.classIndexToStudentsList[index]
            }
        }

        function setClasses() {
            if (vm.isManager) {
                vm.allClasses = vm.currentUser.school.school_classes;
            }
            else if (vm.isTeacher) {
                vm.allClasses = vm.currentUser.teaching_classes;
            }

            vm.allClassesViewOnly = vm.allClasses.slice()
        }


        function setStudentsScoresByCurrentClass() {
            for (var i = 0; i < vm.chosenClass.class_students.length; i++) {
                vm.studentsNames.push(vm.chosenClass.class_students[i].user.first_name);
                vm.studentsScores.push(vm.chosenClass.class_students[i].get_average);
            }
        }

        function setallStudentScores() {
            for (var i = 0; i < vm.allClasses.length; i++) {
                for (var j = 0; j < vm.allClasses[i].class_students.length; j++) {
                    vm.studentsNames.push(vm.allClasses[i].class_students[j].user.first_name);
                    vm.studentsScores.push(vm.allClasses[i].class_students[j].get_average);
                }
            }
        }

        function setStudentScores() {
            if (Object.keys(vm.chosenClass).length > 0) {
                setStudentsScoresByCurrentClass();
            }

            else {
                setallStudentScores();
            }

        }


        function setChartOptions() {
            vm.chartOptions = {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        },
                    }],
                },
                legend: {
                    display: false,
                },
                animationSteps: 15,
            };

        }

        function initChart() {
            vm.chartData = {
                labels: vm.studentsNames, //zir x ave
                datasets: [{
                    label: 'Average Grade',
                    backgroundColor: '#C8E6C9',
                    borderColor: '#1B5E20',
                    // pointRadius: 0,
                    data: vm.studentsScores, //zir y amount
                }],
            };
        }

        function displayChart() {
            var chartElement = document.getElementById('gradesChart').getContext('2d');
            vm.chart = new Chart(chartElement, {type: 'line', data: vm.chartData, options: vm.chartOptions});
            vm.chart.update();
        }


    }
})();