(function () {
    'use static';

    angular
        .module('smsApp')
        .controller('weakStudentController', weakStudentController);

    weakStudentController.$inject = ['weakStudentService'];


    function weakStudentController(weakStudentService) {

        var vm = this;
        vm.activate = activate;
        vm.setWeakStudents = setWeakStudents;
        vm.weakStudentsList = [];
        activate();


        //////////////////////////////

        function activate() {
            weakStudentService.getAllWeakStudents(function (weak_students) {
                setWeakStudents(weak_students);
            })
        }


        function setWeakStudents(weak_students) {
            for (var i = 0; i < weak_students.length; i++) {
                vm.weakStudentsList.push({
                    name: weak_students[i].user.first_name + ' ' + weak_students[i].user.last_name,
                    score: weak_students[i].get_average
                })
            }
        }

    }
})();