(function () {
    'use strict';

    angular
        .module('smsApp')
        .factory('weakStudentService', weakStudentService);


    weakStudentService.$inject = ['$http'];

    /* @ngInject */

    function weakStudentService($http) {
        var service = {
            getAllWeakStudents: getAllWeakStudents

        };

        return service;

        ///////////////

        function getAllWeakStudents(callback) {
            $http.get('/api/v1/sms-app/get_all_weak_students/').then(function (students) {
                callback(students.data);
            });


        }


    }


})();