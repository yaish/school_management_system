(function () {
    'use strict';

    angular
        .module('smsApp')
        .factory('tokenManager', tokenManager);


    tokenManager.$inject = ['$window'];

    /* @ngInject */

    function tokenManager($window) {
        var service = {
            setToken: setToken,
            getToken: getToken,
        };

        return service;

        ///////////////

        function setToken(token) {
            if (token)
                $window.localStorage.setItem('token', token);
            else
                $window.localStorage.removeItem('token');
        }


        function getToken() {
            return $window.localStorage.getItem('token')
        }


    }


})();