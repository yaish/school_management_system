(function () {
    'use strict';

    angular
        .module('smsApp')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$state', 'appService', '$q', '$scope'];


    function LoginController($state, appService, $q, $scope) {

        var vm = this;
        vm.username = '';
        vm.password = '';
        vm.errorMessage = '';
        vm.isStudent = false;
        vm.login = login;
        vm.activate = activate;
        vm.nevigateToMainApp = nevigateToMainApp;

        activate();


        //////////////////////////////


        function activate() {
        }

        function nevigateToMainApp() {
            $scope.$emit('userLoggedIn');
            $state.go('app.graph');
        }

        function login() {
            appService.login(vm.username, vm.password, function (success) {
                if (success) {
                    appService.setConnectedUserFromApi(function (user) {
                        if (user.get_user_tag == "S") {
                            vm.errorMessage = 'Cannot Login with student credentials';
                        }
                        else {
                            nevigateToMainApp();
                        }
                    });
                }
                else {
                    vm.errorMessage = 'Error Credentials';
                }
            });


        }
    }
})();