(function () {
    'use strict';

    angular
        .module('smsApp')
        .controller("appController", appController);

    appController.$inject = ['$state', '$window', '$scope', 'tokenManager', 'appService', '$q', '$timeout'];

    function appController($state, $window, $scope, tokenManager, appService, $q, $timeout) {
        var vm = this;
        vm.isAuthenticated = false;
        vm.username = "";
        vm.flag = false;
        vm.goToApp = '';
        vm.currentTab = 1;
        vm.isManagerAuthenticated = false;
        vm.activate = activate;
        vm.pageNavigation = pageNavigation;
        vm.logout = logout;
        vm.setUserName = setUserName;
        vm.setTab = setTab;
        vm.isSet = isSet;

        activate();


        function activate() {
            checkIfAuthed();

            $scope.$on('userLoggedIn', function (event, data) {
                setUserName();
                vm.isAuthenticated = true;
            });
        }

        function setUserName() {
            var someUser = appService.getUser();
            if (someUser.get_user_tag == "T") {
                vm.username = 'Teacher ' + someUser.user.first_name + ' ' + someUser.user.last_name
            }
            else {
                vm.isManagerAuthenticated = true;
                vm.username = 'Manager ' + someUser.user.first_name + ' ' + someUser.user.last_name
            }
        }


        function checkIfAuthed() {
            if (tokenManager.getToken() != null) {
                appService.setConnectedUserFromApi(function () {
                    $q.when(setUserName()).then(function () {
                        vm.isAuthenticated = true;
                        pageNavigation();
                    });
                })
            }
            else{
                pageNavigation();
            }

        }


        function pageNavigation() {
            if (vm.isAuthenticated) {
                $timeout(function () {
                    $state.go('app.graph');
                }, 0);
            }
            else {
                $timeout(function () {
                    $state.go('login');
                }, 0);
            }

        }

        function setTab(index) {
            vm.currentTab = index;
        }

        function isSet(tabIndex) {
            return vm.currentTab == tabIndex;
        }

        function logout() {
            tokenManager.setToken();
            vm.currentTab = 1;
            vm.isAuthenticated = false;
            vm.isManagerAuthenticated = false;
            $state.go('login');
        }
    }
})
();
